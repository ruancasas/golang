package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const MONITOR = 2
const DELAY = 5

func main() {
	_, fullname, _ := userInfo()
	fmt.Println("Welcome", fullname)
	showIntro()

	for {
		showMenu()
		option := readOption()

		switch option {
		case 1:
			initializeMonitoring()
		case 2:
			showLogs()
		case 0:
			fmt.Println("Exiting...")
			os.Exit(0)
		default:
			fmt.Println("Invalid option")
			os.Exit(-1)
		}
	}
}

func showIntro() {
	version := "1.0"
	fmt.Println("Software version", version)
}

func showMenu() {
	fmt.Println("1 - Start monitoring")
	fmt.Println("2 - Show logs")
	fmt.Println("0 - Exit")
}

func readOption() int {
	var option int
	fmt.Print("Choice a option: ")
	fmt.Scan(&option)
	return option
}

func testSite(site string) {
	resp, _ := http.Get(site)

	if resp.StatusCode == 200 {
		registerLog(site, true, 200)
		fmt.Println("Site: ", site, "loaded successfully!")
	} else if resp.StatusCode == 404 {
		registerLog(site, false, 404)
		fmt.Println("Site", site, "not found")
	} else {
		registerLog(site, false, resp.StatusCode)
		fmt.Println("Site", site, "have a problem. Status Code: ", resp.StatusCode)
	}
}

func initializeMonitoring() {
	fmt.Println("Monitoring...")

	sites := readFile()
	for i := 0; i < MONITOR; i++ {
		for i, site := range sites {
			fmt.Println("Check site", i, ":", site)
			testSite(site)
		}
		fmt.Println("")
		time.Sleep(DELAY * time.Minute)

	}
	fmt.Println("")
}

func userInfo() (string, string, string) {
	fmt.Println("User information")
	var username string
	fmt.Print("Username: ")
	fmt.Scan(&username)
	var fullname string
	fmt.Print("Fullname: ")
	fmt.Scan(&fullname)
	var password string
	fmt.Print("Password: ")
	fmt.Scan(&password)

	return username, fullname, password
}

func readFile() []string {
	var sites []string
	file, err := os.Open("sites")

	if err != nil {
		fmt.Println("Error:", err)
	}

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		line = strings.TrimSpace(line)
		sites = append(sites, line)
		if err == io.EOF {
			break
		}
	}

	file.Close()

	return sites
}

func registerLog(site string, status bool, code int) {
	file, err := os.OpenFile("log", os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
	if err != nil {
		fmt.Print("Error:", err)
	}
	file.WriteString(time.Now().Format("02/01/2006 15:04:05") + " - " + "[" + strconv.Itoa(code) + "] " + site +
		" - online: " + strconv.FormatBool(status) + "\n")
	file.Close()
}

func showLogs() {
	fmt.Println("Showing logs...")

	file, err := ioutil.ReadFile("log")

	if err != nil {
		fmt.Println("Error:", err)
	}

	fmt.Println(string(file))
}
