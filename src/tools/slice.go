package main

import "fmt"

func main() {
	showNames()
	showCapacity()
}

func showNames() {
	names := []string{"Ruan"}
	fmt.Println("My slice contains", len(names), "item")

	names = append(names, "Batman")
	fmt.Println("My slice contains", len(names), "items")
}

func showCapacity() {
	names := []string{"Ruan", "Batman", "Flash"}
	fmt.Println("My slice have", len(names), "items")
	fmt.Println("My slice have capacity to", cap(names), "items")

	names = append(names, "SuperMan")
	fmt.Println("My slice have", len(names), "items")
	fmt.Println("My slice have capacity to", cap(names), "items")
}
