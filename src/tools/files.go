package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	readOneLine()
	m := multipleLines()
	fmt.Println("Multiple lines:", m)
}

func readOneLine() {
	fmt.Println("Read one line")
	file, err := os.Open("test.txt")
	if err != nil {
		fmt.Println("Error:", err)
	}
	reader := bufio.NewReader(file)
	line, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Error:", err)
	}
	fmt.Println(line)
}

func multipleLines() []string {
	var tests []string
	file, err := os.Open("test.txt")

	if err != nil {
		fmt.Println("Error:", err)
	}

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		line = strings.TrimSpace(line)
		tests = append(tests, line)
		if err == io.EOF {
			break
		}
	}

	file.Close()
	return tests
}
