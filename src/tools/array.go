package main

import "fmt"

func main() {
	var names [3]string
	names[0] = "Batman"
	names[1] = "Flask"
	names[2] = "SuperMan"

	for _, n := range names {
		fmt.Println("Name:", n)
	}
}
